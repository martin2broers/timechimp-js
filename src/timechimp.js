class TimeRange {
    /**
     *
     * @param {MiniTime} from
     * @param {MiniTime} until
     */
    constructor(from, until) {
        this._from = from;
        this._until = until;
    }

    /**
     *
     * @param {TimeRange} timeRange
     */
    isInRange(timeRange) {
        const begin = this._from.totalMinutes;
        const end = this._until.totalMinutes;

        if (timeRange.from.totalMinutes > begin && timeRange.from.totalMinutes <= end) {
            return true; //in range
        }
        return timeRange.until.totalMinutes <= end && timeRange.until.totalMinutes > begin;
         //not in range
    }

    /**
     * @returns {MiniTime}
     */
    get from() {
        return this._from;
    }

    /**
     * @returns {MiniTime}
     */
    get until() {
        return this._until;
    }
}


class MiniTime {

    /**
     * @param {string} strTime
     */
    constructor(strTime) {
        this.strTime = strTime;
        this.split = strTime.replace(":", "").split("");
    }

    /**
     * @returns {boolean}
     */
    isValid() {
        const length = this.split.length;
        if (length === 4) {
            const firstPart = parseInt(this.split.slice(0, 2).join(""));
            if (firstPart < 0 || firstPart > 24) {
                return false;
            }
            const secondPart = parseInt(this.split.slice(2, 4).join(""));
            return !(secondPart < 0 || secondPart > 60);

        }
        return false;
    }

    /**
     * @returns {number}
     */
    get hours() {
        return parseInt(this.split.slice(0, 2).join(""));
    }

    /**
     * @returns {number}
     */
    get minutes() {
        return parseInt(this.split.slice(2, 4).join(""));
    }

    /**
     * @returns {number}
     */
    get totalMinutes() {
        return this.getHours() * this.getMinutes();
    }

    /**
     * @returns {string}
     */
    get stringTime() {
        return this.strTime;
    }

    /**
     * @returns {MiniTime}
     */
    static fromSeconds(seconds) {
        return undefined;
    }
}


class TimeChimpHack {


    forEachElement(elements, callback) {
        for (let i = 0; i < elements.length; i++) {
            callback(elements[i]);
        }
    }

    /**
     *
     * @param element
     * @param attr
     * @param attrValue
     * @returns {Promise<HTMLElement>}
     */
    getElementWithAttribute(element, attr, attrValue) {
        return new Promise((resolve, reject) => {
            let found = false;
            this.forEachElement(document.getElementsByTagName(element), element => {
                if (element.getAttribute(attr) === attrValue) {
                    found = true;
                    resolve(element);
                }
            });
            if (!found) {
                reject();
            }
        });
    }

    triggerEvent(el, type) {
        if ('createEvent' in document) {
            // modern browsers, IE9+
            const e = document.createEvent('Event');
            e.initEvent(type, true, true);
            el.dispatchEvent(e);
        } else {
            // IE 8
            const e = document.createEventObject();
            e.eventType = type;
            el.fireEvent('on' + e.eventType, e);
        }
    }

    getDropDown(target) {
        let element = null;
        this.forEachElement(document.getElementsByClassName("ui-select-container ui-select-bootstrap dropdown"), function (dropdown) {
            const model = dropdown.getAttribute("ng-model");
            if (model === "vm.time.customerId" && target === "customer") {
                element = dropdown.getElementsByClassName("btn btn-default form-control ui-select-toggle")[0];
            }
            if (model === "vm.time.projectId" && target === "project") {
                element = dropdown.getElementsByClassName("btn btn-default form-control ui-select-toggle")[0];
            }
            if (model === "vm.time.projectTaskId" && target === "task") {
                element = dropdown.getElementsByClassName("btn btn-default form-control ui-select-toggle")[0];
            }
        });
        return element;
    }

    /**
     * @returns {Promise<void>}
     */
    closeDropDowns() {
        return new Promise((resolve, reject) => {
            document.getElementsByClassName("container c-main ng-scope")[0].click();
            setTimeout(() => {
                resolve();
            }, 200);
        });
    }

    /**
     *
     * @param open
     * @returns {Promise<string[]>}
     */
    async listClients(open = true) {
        if (!open) {
            return Promise.resolve([]);
        }
        return new Promise((resolve, _) => {
            const list = [];
            this.getDropDown("customer").click();
            this.setAllTitlesThenResolve(list, resolve);
        });
    }

    setAllTitlesThenResolve(list, resolve) {
        setTimeout(() => {
            const clients = document.getElementsByClassName("ui-select-choices-row ng-scope");
            this.forEachElement(clients, (element) => {
                const inner = element.getElementsByClassName("ui-select-choices-row-inner")[0];
                const selection = inner.getElementsByTagName("div")[0];
                list.push(selection.getAttribute("title"));
            });
            resolve(list);
        }, 500);
    }

    /**
     *
     * @param customer
     * @param openCustomer
     * @param openProject
     * @returns {Promise<string[]>}
     */
    listProjectsOf(customer, openCustomer = true, openProject = true) {
        if (!openProject) {
            return Promise.resolve([]);
        }
        return new Promise(async (resolve, _) => {
            const list = [];
            await this.selectClient(customer, openCustomer);
            this.getDropDown("project").click();
            this.setAllTitlesThenResolve(list, resolve);
        });
    }

    /**
     *
     * @param customer
     * @param project
     * @param openCustomer
     * @param openProject
     * @returns {Promise<string[]>}
     */
    listTasksOf(customer, project, openCustomer = true, openProject = true) {
        return new Promise(async (resolve, _) => {
            const list = [];
            await this.selectClient(customer, openCustomer);
            await this.selectProject(project, openProject);
            this.getDropDown("task").click();
            this.setAllTitlesThenResolve(list, resolve);
        });
    }


    async createMap() {
        return new Promise(async (resolve, _) => {
            const totalObject = {};

            const listClients = await this.listClients();
            for (let e = 0; e < listClients.length; e++) {
                totalObject[listClients[e]] = {};
                const projects = await this.listProjectsOf(listClients[e]);
                for (let e1 = 0; e1 < projects.length; e1++) {
                    totalObject[listClients[e]][projects[e1]] = await this.listTasksOf(listClients[e], projects[e1]);
                }
            }
            resolve(totalObject);
        });
    }

    selectFromCurrentDropDown(title) {
        return new Promise((resolve, reject) => {
            let found = false;
            const clients = document.getElementsByClassName("ui-select-choices-row ng-scope");
            this.forEachElement(clients, function (element) {
                const inner = element.getElementsByClassName("ui-select-choices-row-inner")[0];
                const selection = inner.getElementsByTagName("div")[0];
                if (selection.getAttribute("title") === title) {
                    element.click();
                    resolve();
                }
            });
            if (!found) {
                reject();
            }
        });
    }

    openDropDown(type, selection) {
        return new Promise((resolve, _) => {
            this.getDropDown(type).click();
            setTimeout(async () => {
                await this.selectFromCurrentDropDown(selection);
                resolve();
            }, 500);
        });
    }

    selectClient(clientName) {
        return this.openDropDown("customer", clientName);
    }

    selectProject(project) {
        return this.openDropDown("project", project);
    }

    selectTask(task) {
        return this.openDropDown("task", task);
    }


    async enterMessage(message) {
        const textArea = await this.getElementWithAttribute("textarea", "ng-model", "vm.time.notes");
        textArea.value = message;
        textArea.innerHtml = message;
    }

    async enterBeginTime(time) {
        const timeInput = await this.getElementWithAttribute("input", "ng-model", "vm.time.startTime");
        timeInput.value = this.transformTimeToTcTime(time).stringTime;
        this.setInputTime(timeInput, time);
        this.triggerInputEvents(timeInput);
        timeInput.click();
    }

    async enterEndTime(time) {
        const timeInput = await this.getElementWithAttribute("input", "ng-model", "vm.time.endTime");
        this.setInputTime(timeInput, time);
        this.triggerInputEvents(timeInput);
        timeInput.click();
    }

    /**
     * @private
     * @param timeInput
     */
    triggerInputEvents(timeInput) {
        this.triggerEvent(timeInput, 'input');
        this.triggerEvent(timeInput, 'change');
        this.triggerEvent(timeInput, 'focus');
        this.triggerEvent(timeInput, 'keydown');
        this.triggerEvent(timeInput, 'keypress');
        this.triggerEvent(timeInput, 'keyup');
        this.triggerEvent(timeInput, 'blur');
    }

    /**
     * @private
     * @param timeInput
     * @param time
     */
    setInputTime(timeInput, time) {
        timeInput.value = this.transformTimeToTcTime(time).stringTime;
    }

    async submit() {
        const timeInput = await this.getElementWithAttribute("button", "type", "submit");
        timeInput.click();
    }

    transformTimeToTcTime(time) {
        const tcTime = new MiniTime(time);
        if (!tcTime.isValid()) {
            throw "Not a valid time"
        }
        return tcTime;
    }

    /**
     *
     * @returns {TimeRange[]}
     */
    getAllTimes() {
        const times = [];
        this.forEachElement(document.getElementsByClassName("list-group-item ng-scope"), element => {
            if(!element.classList.contains("time-day-no-hours")){
                const ngLabels = element.getElementsByClassName("task")[0].getElementsByClassName("ng-binding");
                const element = this.getFirstBookedTimeString(ngLabels)
                const beginAndEndTime = element.innerText.split("|")[0].trim().split("-");
                times.push(new TimeRange(new MiniTime(beginAndEndTime[0]), new MiniTime(beginAndEndTime[1])));
            }
        });
        return times;
    }

    getFirstBookedTimeString(items){
        this.forEachElement(items, ng_binding => {
            if(!ng_binding.classList.contains("label")){
                return ng_binding;
            }
        })
    }

    /**
     * @param {MiniTime} from
     * @param {MiniTime} until
     * @returns {TimeRange[]}
     */
    getAvailableTimes(from, until){
        const availableTimeSlots = [];
        const minutesBooked = {};
        const blockedTimes = this.getAllTimes().sort(e => e.from.totalMinutes); //sorting from until
        for(let i = from.totalMinutes; i < until.totalMinutes; i++){
            minutesBooked[i](true);
        }
        for(const booked of blockedTimes){
            for(let i = booked.from.totalMinutes; i < booked.until.totalMinutes; i++){
                minutesBooked[i](false);
            }
        }
        let avFrom = null;
        for(let i = from.totalMinutes; i < until.totalMinutes; i++){
            if(avFrom == null && minutesBooked[i]){
                //
                avFrom = MiniTime.fromSeconds(i);
            }
            if(avFrom != null && !minutesBooked[i]){
                const until = MiniTime.fromSeconds(i); //now we have a full range and can set it
                availableTimeSlots.push(new TimeRange(avFrom, until));
            }
        }
        return availableTimeSlots;
    }

    async book(clientName, projectName, taskName, from, until, message, fillInGaps = true) {
        if (fillInGaps) {
            const times = this.getAllTimes();
            const currentTimeRange = new TimeRange(new MiniTime(from), new MiniTime(until));
            let canJustFillIn = true;
            for (const timeRange of times) {
                if (timeRange.isInRange(currentTimeRange)) {
                    canJustFillIn = false;
                }
            }
            if (canJustFillIn) {
                return this.book(clientName, projectName, taskName, from, until, message, false);
            } else {
                //this is where shit get complicated
                const availableTimeSlots = this.getAvailableTimes(from, until);
            }
        } else {
            await this.selectClient(clientName);
            await this.selectProject(projectName);
            await this.selectTask(taskName);
            await this.enterBeginTime(from);
            await this.enterEndTime(until);
            await this.enterMessage(message);
            await this.submit();
        }
    }
}

const tc = new TimeChimpHack();

window.tc = tc;
